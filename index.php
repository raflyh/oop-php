<?php
    //require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("Shaun");

    echo "Name : $sheep->name <br>"; // "shaun"
    echo "Legs : $sheep->legs<br>"; // 4
    echo "Cold Blooded : $sheep->cold_blooded<br><br>"; // "no"

    $sarimin = new Ape("Sapri");

    echo "Name : $sarimin->name <br>"; // "shaun"
    echo "Legs : $sarimin->legs<br>"; // 2
    echo "Cold Blooded : $sarimin->cold_blooded<br>"; // "no"
    $sarimin->yell();
    
    $bangkong = new Frog("Gama");

    echo "Name : $bangkong->name <br>"; // "shaun"
    echo "Legs : $bangkong->legs<br>"; // 4
    echo "Cold Blooded : $bangkong->cold_blooded<br>"; // "no"
    $bangkong->jump();
